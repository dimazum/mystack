﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomStack
{
    class CustomStack<T> : ICustomStack<T>
    {
        int capacity = 5;
        T[] steck;
        int count = 0;
        public CustomStack()
        {
            steck = new T[capacity];
        }

        public void Push(T a)
        {
            if (count < capacity)
            {
                steck[count] = a;
                count++;
            }
            else
            {
                T[] temp = steck;
                capacity += 5;
                steck = new T[capacity];
                temp.CopyTo(steck, 0);
                steck[count] = a;
                count++;
            }
        }
        public T Pop()
        {
            if (count > 0)
            {
                T a = steck[count - 1];
                count--;
                return a;
            }
            else
            {
                throw new InvalidOperationException("стек пуст");
            }
        }
    }
}
