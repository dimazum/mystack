﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomStack
{
    interface ICustomStack<T>
    {
        void Push(T a);
        T Pop();
    }
}
