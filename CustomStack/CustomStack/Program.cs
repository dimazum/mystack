﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomStack
{
    class Program
    {
        static void Main(string[] args)
        {

            ICustomStack<int> customStack = new CustomStack<int>();

            Random rnd = new Random();
            for (int i = 0; i < 5; i++)
            {
                //customStack.Push(rnd.Next(1, 9));
                customStack.Push(i);
            }
            while (!Console.ReadLine().Equals("exit"))
            {
                Console.WriteLine(customStack.Pop());
            }
        }
    }
}
